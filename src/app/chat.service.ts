import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

export interface WebSocketMessage {
  text: string;
  quickReplies: string [];
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public socketMessage: Subject<WebSocketMessage>;

  constructor(private wsService: WebsocketService) { 
    this.socketMessage = <Subject<WebSocketMessage>> wsService
    .connect('ws://localhost:3120')
    .map(( response: MessageEvent): WebSocketMessage => {
      let data = JSON.parse(response.data);
      return {
        text: data.text,
        quickReplies: data.quickReplies
      }
    })
  }
}
