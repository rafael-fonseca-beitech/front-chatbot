import { Component, OnInit } from '@angular/core';
import { ChatService } from './../chat.service';
import Chatbot from './../models/Chatbot';
import Message  from './../models/Message';
import { WebSocketMessage } from 'rxjs/internal/observable/dom/WebSocketSubject';

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.css']
})
export class ChatboxComponent implements OnInit {

  clientImageUrl: string = 'https://cdn.iconscout.com/icon/premium/png-256-thumb/add-client-606394.png';
  botImageUrl: string = 'https://cdn3.iconfinder.com/data/icons/customer-support-7/32/40_robot_bot_customer_help_support_automatic_reply-512.png';

  initialMessages: Array<Message> = [];
  initialQuickReplies: Array<string> = [];
  chatbot: Chatbot= {isMinimized: true, isClosed: false, text: '', messageList: this.initialMessages, quickReplies: this.initialQuickReplies};

  constructor (private chatService: ChatService){
    chatService.socketMessage.subscribe(msg => {
      console.log("Response from server ", msg);
      var now: Date = new Date();
      let hour: string = now.getHours().toString() + ':' + now.getMinutes().toString();
      let day: string = now.getDay().toString() + '/' + (now.getMonth() + 1).toString() + '/' + now.getFullYear().toString();
      let message: Message = new Message('bot', msg.text, day , hour);
      this.chatbot.messageList.push(message);
      this.chatbot.quickReplies = msg.quickReplies;
    })
  }

  public toggle(){
    this.chatbot.isMinimized = !this.chatbot.isMinimized;
  }

  public close(){
    this.chatbot.isClosed = true;
  }

  public sendMessage(){
    var now: Date = new Date();
    let hour: string = now.getHours().toString() + ':' + now.getMinutes().toString();
    let day: string = now.getDay().toString() + '/' + (now.getMonth() + 1).toString() + '/' + now.getFullYear().toString();
    let message: Message = new Message('client', this.chatbot.text, day , hour);
    let websocketMessage = {
      "text": this.chatbot.text,
      "quickReplies": []
    };
    this.chatbot.messageList.push(message);
    this.chatService.socketMessage.next(websocketMessage);
  }

  public sendQuickReply(reply: string){
    var now: Date = new Date();
    let hour: string = now.getHours().toString() + ':' + now.getMinutes().toString();
    let day: string = now.getDay().toString() + '/' + (now.getMonth() + 1).toString() + '/' + now.getFullYear().toString();
    let message: Message = new Message('client', reply, day , hour);
    let websocketMessage = {
      "text": reply,
      "quickReplies": []
    }
    this.chatbot.messageList.push(message);
    this.chatService.socketMessage.next(websocketMessage);
  }

  ngOnInit() {
  }

}
