import Message from './Message';

export default class Chatbot {
    isMinimized: boolean;
    isClosed: boolean;
    text: string;
    messageList: Message [];
    quickReplies: string [];
}