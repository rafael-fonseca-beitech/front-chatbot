export default class Message {

    owner: string;
    text: string;
    hour: string;
    day: string;

    constructor(owner: string, text: string, hour: string, day: string){
        this.owner = owner;
        this.text = text;
        this.hour = hour;
        this.day = day;
    }
    
}